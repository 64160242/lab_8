package com.phil.week8;

public class TriangleApp {
    public static void main(String[] args) {
        Triangle trianglet1 = new Triangle(5, 5, 6);
        trianglet1.printAreaTriangle();
        trianglet1.printPerimeterTriangle();
    }
}
